({
	drawConference : function(component) {
	   
       debugger;
       var globalID = component.getGlobalId();
       console.log(component.getGlobalId());
      
       var obj =  component.get("v.wrapperObj");
       var parentdiv = document.getElementById(globalID); 
       parentdiv.addEventListener("click",$A.getCallback(function myFunction(component,event){
       var sObjectEvent = $A.get("e.c:EditLayoutEvent");
       		sObjectEvent.setParams({
            "message": obj
            })
        sObjectEvent.fire();
        })); 
       var totalNoOfSeats =  component.get("v.wrapperObj.NumberOfSeats");
       //var horizontalSeats = component.get("v.wrapperObj.HorizontalSeatsTop");
	   var horizontalSeats = component.get("v.wrapperObj.HorizontalSeatsBottom");   
       
       var numberOfSeatsOnLeftSide = Math.round((totalNoOfSeats - horizontalSeats*2)/2) ;
       var numberOfSeatsOnRightSide = totalNoOfSeats - numberOfSeatsOnLeftSide- horizontalSeats*2;
        
       // left Side seating arrangement start 
        
       var getLeftSeats = document.getElementById(globalID+'_leftSeating');
       var leftSideSeats = document.createElement('ul');
       leftSideSeats.id = globalID + '_leftSideUl';
       getLeftSeats.appendChild(leftSideSeats);
       var i=0; 
       for(i=0;i<numberOfSeatsOnLeftSide;i++){
           
           var leftSideSeat =  document.createElement('li');
           leftSideSeat.id = globalID + '_leftSideSeat'+i;
           leftSideSeat.className = 'chair';
           var seatNumber = document.createElement('p');
            seatNumber.innerHTML = 'S'+i;
            seatNumber.className = 'p';
           leftSideSeat.appendChild(seatNumber);
           $(leftSideSeat).on('dragover', function (event) {
                console.log('ondragover0 called...');
                event.preventDefault();
            });
           
            $(leftSideSeat).on('drop', function (event) {
                debugger;
                var sObjectEvent1;
                var obje = {};
          		 var status = $(this).attr('class');
                if(status.indexOf("booked") <= 0){
                    var listitem = '';
                    if(event.originalEvent != undefined && event.originalEvent != null){
                    	listitem = event.originalEvent.dataTransfer.getData("obj");    
                        obje = JSON.parse(listitem);
                    }
                    var contactId = obje.Id;
                    var contactName = obje.Name;
                    component.set("v.afterRemove",contactName);
                    var seatNo = $(this).find('p').text();
                    var tableNo = component.get("v.wrapperObj.tableName");
                    var seat = tableNo + '_'+  seatNo;
                    var conSeat = contactName + '[' + seat + ']';
                    component.set("v.afterRemove",conSeat);
                    $(this).addClass('booked');
                    $(this).addClass('bookedseat');
                    $(this).attr('data','booked');
                    $(this).attr('contact',contactId);
                    var conList = component.get("v.contactList");
                    for(var i = 0;i<conList.length;i++){
                        if(conList[i].Name == contactName){
                            conList[i].Name = contactName + '[' + seat + ']';
                            conList[i].Title = seat;
                        }
                    }
                    $(this).append('<span Id="' + seat + '" class="orangeBox">X</span>');
                    
                    var spanId = document.getElementById(seat);
                    console.log('spanId'+spanId);
                    
                    
                     $(this).hover(
                      function () {
                        $(spanId).show();
                      }, 
                      function () {
                        $(spanId).hide();
                      }
                    );
                    
					$(spanId).on('click' ,function(){
                    	alert('Hello');
                        debugger;
                        $(this).parent().removeClass('bookedseat');
                        $(this).parent().removeClass('booked');
                        $(this).parent().removeAttr('data');
                        $(this).parent().unbind('mouseenter');
                          $(this).parent().removeClass('orangeBox');
                        $(spanId).remove();

                        var a = $(this).parent();
                        var contactID = a.attr("contact")
                        var conList = component.get("v.contactList");
                    	for(var i = 0;i<conList.length;i++){
                        	if(conList[i].Id == contactID){
                            	//conList[i].Name = 'WWWWWW';//Mustafa will look into it
                            	//conList[i].Title = '';
                        	}
                    	}
                        //$(this).parent().trigger("drop");
                                                 
                         sObjectEvent1 = component.getEvent("getAssignSeat");
                    
                            sObjectEvent1.setParams({
                                "contactId":contactId,
                                "seat" :seat,
                                "tableNo":tableNo,
                                "status":"Cancelled"
                               })
                           sObjectEvent1.fire();
                      });
                     sObjectEvent1 = component.getEvent("getAssignSeat");
                    var status = "Registered";
                    sObjectEvent1.setParams({
                        "contactId":contactId,
                        "seat" :seat,
                        "tableNo":tableNo,
                        "status":status
                       })
                   sObjectEvent1.fire();
                }
                else{
                    alert("Sorry ! already booked...");
                }
             
            });
           $("ul[id='"+globalID+"_leftSideUl"+"']").append(leftSideSeat);
           
		}
        var lSeats = document.getElementById(globalID+'_leftSeating');
        lSeats.style.height = 30*i+'px';
        // left Side seating arrangement end
      
      // Horizontal seat bottom arrangement Start
       var getHorizontalSeats = document.getElementById(globalID+'_horizontalSeatingBottom');
       var horizontalSideSeats = document.createElement('ul');
       horizontalSideSeats.id = globalID + '_horizontalBottomUl';
       horizontalSideSeats.style.position = 'relative';
       horizontalSideSeats.style.top = 4+'px';
       getHorizontalSeats.appendChild(horizontalSideSeats);
       var bottom=0;
       var p=0;
       for(bottom=0;bottom<horizontalSeats;bottom++){
             p=i+bottom;
           var horizontalSeat =  document.createElement('li');
           horizontalSeat.id = globalID + '_horizontalBottomSeat'+bottom;
           horizontalSeat.className = 'chair';
            var seatNumber = document.createElement('p');
            seatNumber.innerHTML = 'S'+p;
            seatNumber.className = 'p';
            horizontalSeat.appendChild(seatNumber);
           $(horizontalSeat).on('dragover', function (event) {
                console.log('ondragover0 called...');
                event.preventDefault();
            });
           
            $(horizontalSeat).on('drop', function (event) {
                debugger;
                var sObjectEvent1;
                var obje = {};
          		 var status = $(this).attr('class');
                if(status.indexOf("booked") <= 0){
                    var listitem = '';
                    if(event.originalEvent != undefined && event.originalEvent != null){
                    	listitem = event.originalEvent.dataTransfer.getData("obj");    
                        obje = JSON.parse(listitem);
                    }
                    var contactId = obje.Id;
                    var contactName = obje.Name;
                    component.set("v.afterRemove",contactName);
                    var seatNo = $(this).find('p').text();
                    var tableNo = component.get("v.wrapperObj.tableName");
                    var seat = tableNo + '_'+ seatNo;
                    var conSeat = contactName + '[' + seat + ']';
                    component.set("v.afterRemove",conSeat);
                    $(this).addClass('booked');
                    $(this).addClass('bookedseat');
                    $(this).attr('data','booked');
                    $(this).attr('contact',contactId);
                    var conList = component.get("v.contactList");
                    for(var i = 0;i<conList.length;i++){
                        if(conList[i].Name == contactName){
                            conList[i].Name = contactName + '[' + seat + ']';
                            conList[i].Title = seat;
                        }
                    }
                    $(this).append('<span Id="' + seat + '" class="orangeBox">X</span>');
                    
                    var spanId = document.getElementById(seat);
                    console.log('spanId'+spanId);
                    
                    $(spanId).addClass("orangeBox");
                     $(this).hover(
                      function () {
                        $(spanId).show();
                      }, 
                      function () {
                        $(spanId).hide();
                      }
                    );
                    
					$(spanId).on('click' ,function(){
                    	alert('Hello');
                        debugger;
                        $(this).parent().removeClass('bookedseat');
                        $(this).parent().removeClass('booked');
                        $(this).parent().removeAttr('data');
                        $(this).parent().unbind('mouseenter');
                        $(this).parent().removeClass('orangeBox');
                        $(spanId).remove();
                        var a = $(this).parent();
                        var contactID = a.attr("contact")
                        var conList = component.get("v.contactList");
                    	for(var i = 0;i<conList.length;i++){
                        	if(conList[i].Id == contactID){
                            	//conList[i].Name = 'WWWWWW';//Mustafa will look into it
                            	//conList[i].Title = '';
                        	}
                    	}
                        //$(this).parent().trigger("drop");
                                                 
                         sObjectEvent1 = component.getEvent("getAssignSeat");
                    
                            sObjectEvent1.setParams({
                                "contactId":contactId,
                                "seat" :seat,
                                "tableNo":tableNo,
                                "status":"Cancelled"
                               })
                           sObjectEvent1.fire();
                      });
                     sObjectEvent1 = component.getEvent("getAssignSeat");
                    var status = "Registered";
                    sObjectEvent1.setParams({
                        "contactId":contactId,
                        "seat" :seat,
                        "tableNo":tableNo,
                        "status":status
                       })
                   sObjectEvent1.fire();
                }
                else{
                    alert("Sorry ! already booked...");
                }
             
            });
           $("ul[id='"+globalID+"_horizontalBottomUl"+"']").append(horizontalSeat);
		}
        var hSeats = document.getElementById(globalID+'_horizontalSeatingBottom');
        hSeats.style.width = 10*(bottom+1)+24*bottom+'px';
        // Horizontal seat bottom arrangement end
         // Right Side seating arrangement start
	   var getRightSeats = document.getElementById(globalID+'_rightSeating');
       var rightSideSeats = document.createElement('ul');
       rightSideSeats.id = globalID + '_rightSideUl';
       getRightSeats.appendChild(rightSideSeats);
       var j=0;
       for(j=0;j<numberOfSeatsOnRightSide;j++){
            var r= p+numberOfSeatsOnRightSide-j;
           var rightSideSeat =  document.createElement('li');
           rightSideSeat.id = globalID + '_rightSideSeat'+j;
           rightSideSeat.className = 'chair';
            var seatNumber = document.createElement('p');
            seatNumber.innerHTML = 'S'+r;
            seatNumber.className = 'p';
            rightSideSeat.appendChild(seatNumber);
           $(rightSideSeat).on('dragover', function (event) {
                console.log('ondragover0 called...');
                event.preventDefault();
            });
           
            $(rightSideSeat).on('drop', function (event) {
                debugger;
                var sObjectEvent1;
                var obje = {};
          		 var status = $(this).attr('class');
                if(status.indexOf("booked") <= 0){
                    var listitem = '';
                    if(event.originalEvent != undefined && event.originalEvent != null){
                    	listitem = event.originalEvent.dataTransfer.getData("obj");    
                        obje = JSON.parse(listitem);
                    }
                    var contactId = obje.Id;
                    var contactName = obje.Name;
                    component.set("v.afterRemove",contactName);
                    var seatNo = $(this).find('p').text();
                    var tableNo = component.get("v.wrapperObj.tableName");
                    var seat = tableNo + '_'+ seatNo;
                    var conSeat = contactName + '[' + seat + ']';
                    component.set("v.afterRemove",conSeat);
                    $(this).addClass('booked');
                    $(this).addClass('bookedseat');
                    $(this).attr('data','booked');
                    $(this).attr('contact',contactId);
                    var conList = component.get("v.contactList");
                    for(var i = 0;i<conList.length;i++){
                        if(conList[i].Name == contactName){
                            conList[i].Name = contactName + '[' + seat + ']';
                            conList[i].Title = seat;
                        }
                    }
                    $(this).append('<span Id="' + seat + '" class="orangeBox">X</span>');
                    
                    var spanId = document.getElementById(seat);
                    console.log('spanId'+spanId);
                    
                     $(this).hover(
                      function () {
                        $(spanId).show();
                      }, 
                      function () {
                        $(spanId).hide();
                      }
                    );
                    
					$(spanId).on('click' ,function(){
                    	alert('Hello');
                        debugger;
                        $(this).parent().removeClass('bookedseat');
                        $(this).parent().removeClass('booked');
                        $(this).parent().removeAttr('data');
                        $(this).parent().unbind('mouseenter');
                         $(this).parent().removeClass('orangeBox');
                        $(spanId).remove();

                        var a = $(this).parent();
                        var contactID = a.attr("contact")
                        var conList = component.get("v.contactList");
                    	for(var i = 0;i<conList.length;i++){
                        	if(conList[i].Id == contactID){
                            	//conList[i].Name = 'WWWWWW';//Mustafa will look into it
                            	//conList[i].Title = '';
                        	}
                    	}
                        //$(this).parent().trigger("drop");
                                                 
                         sObjectEvent1 = component.getEvent("getAssignSeat");
                    
                            sObjectEvent1.setParams({
                                "contactId":contactId,
                                "seat" :seat,
                                "tableNo":tableNo,
                                "status":"Cancelled"
                               })
                           sObjectEvent1.fire();
                      });
                     sObjectEvent1 = component.getEvent("getAssignSeat");
                    var status = "Registered";
                    sObjectEvent1.setParams({
                        "contactId":contactId,
                        "seat" :seat,
                        "tableNo":tableNo,
                        "status":status
                       })
                   sObjectEvent1.fire();
                }
                else{
                    alert("Sorry ! already booked...");
                }
             
            });
           $("ul[id='"+globalID+"_rightSideUl"+"']").append(rightSideSeat);
           
		}
        
        var lSeats = document.getElementById(globalID+'_rightSeating');
        lSeats.style.height = 30*i+'px';
        lSeats.style.marginTop = (30*i)*(-1)+ 1 +'px';
        lSeats.style.marginLeft = 50+ 10*(horizontalSeats+1) +(24*horizontalSeats)+'px';
     
      // Right Side seating arrangement end
       // Horizontal seat top arrangement Start
       var getHorizontalTopSeats = document.getElementById(globalID+'_horizontalSeatingTop');
       var horizontalTopSideSeats = document.createElement('ul');
       horizontalTopSideSeats.id = globalID + '_horizontalTopUl';
       horizontalTopSideSeats.className = 'horizontalSeatTop';
       horizontalTopSideSeats.style.position = 'relative';
       horizontalTopSideSeats.style.top = -27+'px'; 
       getHorizontalTopSeats.appendChild(horizontalTopSideSeats);
       var top=0;
       for(top=0;top<horizontalSeats;top++){
           var s= r+numberOfSeatsOnRightSide+horizontalSeats-top-1;
           var horizontalTopSeat =  document.createElement('li');
           horizontalTopSeat.id = globalID + '_horizontalTopSeat'+top;
           horizontalTopSeat.className = 'chair';
           var seatNumber = document.createElement('p');
            seatNumber.innerHTML = 'S'+s;
            seatNumber.className = 'p';
            horizontalTopSeat.appendChild(seatNumber);
           $(horizontalTopSeat).on('dragover', function (event) {
                console.log('ondragover0 called...');
                event.preventDefault();
            });
           
            $(horizontalTopSeat).on('drop', function (event) {
                debugger;
                var sObjectEvent1;
                var obje = {};
          		 var status = $(this).attr('class');
                if(status.indexOf("booked") <= 0){
                    var listitem = '';
                    if(event.originalEvent != undefined && event.originalEvent != null){
                    	listitem = event.originalEvent.dataTransfer.getData("obj");    
                        obje = JSON.parse(listitem);
                    }
                    var contactId = obje.Id;
                    var contactName = obje.Name;
                    component.set("v.afterRemove",contactName);
                    var seatNo = $(this).find('p').text();
                    var tableNo = component.get("v.wrapperObj.tableName");
                    var seat =tableNo + '_'+ seatNo;
                    var conSeat = contactName + '[' + seat + ']';
                    component.set("v.afterRemove",conSeat);
                    $(this).addClass('booked');
                    $(this).addClass('bookedseat');
                    $(this).attr('data','booked');
                    $(this).attr('contact',contactId);
                    var conList = component.get("v.contactList");
                    for(var i = 0;i<conList.length;i++){
                        if(conList[i].Name == contactName){
                            conList[i].Name = contactName + '[' + seat + ']';
                            conList[i].Title = seat;
                        }
                    }
                    $(this).append('<span Id="' + seat + '" class="orangeBox">X</span>');
                    
                    var spanId = document.getElementById(seat);
                    console.log('spanId'+spanId);
                    
                    
                     $(this).hover(
                      function () {
                        $(spanId).show();
                      }, 
                      function () {
                        $(spanId).hide();
                      }
                    );
                    
					$(spanId).on('click' ,function(){
                    	alert('Hello');
                        debugger;
                        $(this).parent().removeClass('bookedseat');
                        $(this).parent().removeClass('booked');
                        $(this).parent().removeAttr('data');
                        $(this).parent().unbind('mouseenter');
                         $(this).parent().removeClass('orangeBox');
                        $(spanId).remove();

                        var a = $(this).parent();
                        var contactID = a.attr("contact")
                        var conList = component.get("v.contactList");
                    	for(var i = 0;i<conList.length;i++){
                        	if(conList[i].Id == contactID){
                            	//conList[i].Name = 'WWWWWW';//Mustafa will look into it
                            	//conList[i].Title = '';
                        	}
                    	}
                        //$(this).parent().trigger("drop");
                                                 
                         sObjectEvent1 = component.getEvent("getAssignSeat");
                    
                            sObjectEvent1.setParams({
                                "contactId":contactId,
                                "seat" :seat,
                                "tableNo":tableNo,
                                "status":"Cancelled"
                               })
                           sObjectEvent1.fire();
                      });
                     sObjectEvent1 = component.getEvent("getAssignSeat");
                    var status = "Registered";
                    sObjectEvent1.setParams({
                        "contactId":contactId,
                        "seat" :seat,
                        "tableNo":tableNo,
                        "status":status
                       })
                   sObjectEvent1.fire();
                }
                else{
                    alert("Sorry ! already booked...");
                }
             
            });
           $("ul[id='"+globalID+"_horizontalTopUl"+"']").append(horizontalTopSeat);
		}
        var hTopSeats = document.getElementById(globalID+'_horizontalSeatingTop');
        hTopSeats.style.width = 10*(top+1)+24*top+'px';
        hTopSeats.style.marginTop = (30*i)*(-1)+'px';
        // Horizontal seat top arrangement end
	}
})