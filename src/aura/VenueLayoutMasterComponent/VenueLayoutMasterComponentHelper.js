({
   doInitHelper: function(component, event, helper){
    
     var action = component.get("c.getVenueLayoutType");
        var venueLayoutId = component.get("v.eventvenueId");
        var reRenderFlag = component.get("v.reRenderFlag");
         var wrapperRender = component.get("v.wrapperRender");
        console.log("eventID" + component.get("v.eventID"));
        action.setParams({ "venueLayoutId": venueLayoutId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'ERROR') {
                console.log(a.getError());
            } else if (state === 'SUCCESS') {
                debugger;
                if(reRenderFlag == "false"){
                component.set("v.wrapperList", response.getReturnValue());
                console.log(component.get("v.wrapperList"));
                }else{
                  component.set("v.wrapperList", wrapperRender);  
                }
                var tableList = response.getReturnValue().tables;
                if(tableList.length != 0){
                     conferenceType = response.getReturnValue().tables[0].defaultConferenceType != null ? response.getReturnValue().tables[0].defaultConferenceType : ''; 
                }else{
                    conferenceType = '';
                }
                console.log("tableList" + tableList);
                var rowList = [];
                var columnList = [];
                var columns = response.getReturnValue().SeatsPerRow;
                var rows = response.getReturnValue().numberOfTables / columns;
                //if(rows < rows+.5 ){
                //    rows++;
                //}
                var defaultlayoutType = response.getReturnValue().defaultlayoutType;
                var totalSeats = response.getReturnValue().totalSeats;
                var SeatsPerRow = response.getReturnValue().SeatsPerRow;
                var Sections = response.getReturnValue().Sections;
                for (var i = 0; i < rows; i++) {
                    rowList.push(i);
                }
                for (var j = 0; j < columns; j++) {
                    columnList.push(j);
                }
                component.set("v.numberOfRows", rowList);
                component.set("v.numberOfColumns", columnList);
                var rowListData = [];
                var counter = 1;
                for (var row = 0; row < rows; row++) {
                    for (var column = 0; column < columns; column++) {
                        if (counter <= tableList.length) {
                            rowListData.push({ "rowNo": row, "colNo": column, "table": tableList[counter - 1], "defaultlayoutType": defaultlayoutType +conferenceType, "totalSeats": totalSeats, "SeatsPerRow": SeatsPerRow, "Sections": Sections })
                            counter++;
                        }
                    }
                }
                console.log(rowListData);
                component.set("v.tempList", rowListData);
               
            }
			
			var body = document.getElementById('body');
			var outer = document.getElementById('outer');
			var zoomIn = document.getElementById('zoomIn');
			var zoomOut = document.getElementById('zoomOut');
			var reset = document.getElementById('reset');
			var currentZoom = 1.0;
			
			$(zoomIn).click(function ZoomIn (event) {
			
				$(outer).animate({ 'zoom': currentZoom += .1 }, 'slow');
			});

			$(zoomOut).click(function  ZoomOut (event) {
			
				 $(outer).animate({ 'zoom':  currentZoom -= .1 }, 'slow');
			});
			$(reset).click(function () {
				currentZoom = 1.0
				$(outer).animate({ 'zoom': 1 }, 'slow');
			 });
			
        });
		
		
        $A.enqueueAction(action);
   },
   
   showModal : function(component) {
        
        document.getElementById("modalId").style.display = "block";
     
    },
    
    
    eventRegistrationContact : function(component,event,helper){
          var eventvenueId = component.get("v.eventvenueId");
        console.log('Init venueLayoutController');
        //component.set("v.flag",true);
     
        var action = component.get("c.getEventRegistrationContact");
        action.setParams({ "eventvenueId" : eventvenueId});
        
        action.setCallback(this, function(response) {
            debugger;
            var state = response.getState();
            var results = response.getReturnValue();
            if (state === "SUCCESS") {
             
                 console.log('results'+results);
                
                 var custs = [];
                 for ( key in results ) {
                    custs.push({value:results[key], key:key});
                }
                component.set("v.eventRegistrationList", custs);
               
            }
            
            
            
        });
        $A.enqueueAction(action);
      
    },
     handleAssignSeat : function(cmp, event) {
        debugger;
       var contactId = event.getParam("contactId");
       var seatNo = event.getParam("seat");
       var tableNo = event.getParam("tableNo");
       var status = event.getParam("status");
       var eventvenueId = cmp.get("v.eventvenueId");
       var fieldLst = cmp.get("v.fieldList");
       if(fieldLst.length!=0 && status == "Cancelled"){
         for(var i =0 ; i<fieldLst.length;i++){
          if(fieldLst[i].Contact__c == contactId ){
             fieldLst.splice(i,1);
            }
           }
        }
         if(status == "Registered"){
            fieldLst.push({'Contact__c':contactId,
                        'Seat__c':seatNo,
                        'Event_Venue__c':eventvenueId,
                        'Status__c':status}); 

               console.log("fieldList" + fieldLst);
               cmp.set("v.fieldList",fieldLst);   
            }
              
        
         console.log("fieldList length" + fieldLst.length);
        },
    saveAllocatedMembers : function(component,event){
        debugger;
       var fieldList = component.get("v.fieldList");
       
                var action = component.get("c.saveSeatAllocatedMembers");
       action.setParams({ "fieldList" : JSON.stringify(fieldList)
                         });
        
        action.setCallback(this, function(response) {
            debugger;
            var state = response.getState();
            var results = response.getReturnValue();
          
        });
        $A.enqueueAction(action);
                 
            }
     
    

})