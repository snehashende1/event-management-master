({
    doInit : function(component, event, helper) {
        debugger;	
		var action = component.get("c.getContactList");
        action.setParams({ "campaignId" : component.get("v.recordId")});
        action.setCallback(this, function(response){
        	var state = response.getState();
            if(state  === "SUCCESS"){
                component.set("v.contactList", response.getReturnValue());
            }			
        });
        $A.enqueueAction(action);
	},
    
    allowDrop: function(cmp, event, helper){
        event.preventDefault();
    },
    
    drag: function(cmp, event, helper){
      
		helper.drag(cmp, event);
    },
    
    ondragend: function(component, event, helper) {
        debugger;
        helper.ondragend(component, event);
        
	}
})