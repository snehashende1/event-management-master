({
    createOpenConference : function(component) {
        debugger;
        var globalID = component.getGlobalId();
        console.log(component.getGlobalId());
        
        var totalNoOfSeats =  100;//component.get("v.wrapperObj.NumberOfSeats");
        var horizontalSeats = 6;//component.get("v.wrapperObj.HorizontalSeats");
        var numberOfSeatsOnLeftSide = Math.round((totalNoOfSeats - horizontalSeats*2)/2)
        var numberOfSeatsOnRightSide = totalNoOfSeats-numberOfSeatsOnLeftSide- horizontalSeats*2;
        
        
        
        /****** vertical left Side seating arrangement start *******/
        
        var getLeftSeats = document.getElementById(globalID+'_leftSeating');
        var leftSideSeats = document.createElement('ul');
        leftSideSeats.id = globalID + '_leftSideUl';
        getLeftSeats.appendChild(leftSideSeats);
        var i=0; 
        var q=0;
        for(i=0;i<numberOfSeatsOnLeftSide;i++){
            q=i;
            var leftSideSeat =  document.createElement('li');
            leftSideSeat.id = globalID + '_leftSideSeat'+i;
            leftSideSeat.className = 'chair';
            $(leftSideSeat).on('dragover', function (event) {
                console.log('ondragover0 called...');
                event.preventDefault();
            });
           
            $(leftSideSeat).on('drop', function (event) {
                debugger;
                var sObjectEvent1;
                var obje = {};
          		 var status = $(this).attr('class');
                if(status.indexOf("booked") <= 0){
                    var listitem = '';
                    if(event.originalEvent != undefined && event.originalEvent != null){
                    	listitem = event.originalEvent.dataTransfer.getData("obj");    
                        obje = JSON.parse(listitem);
                    }
                    var contactId = obje.Id;
                    var contactName = obje.Name;
                    component.set("v.afterRemove",contactName);
                    var seatNo = $(this).find('p').text();
                    var tableNo = component.get("v.wrapperObj.tableName");
                    var seat =tableNo + '_'+ seatNo;
                    var conSeat = contactName + '[' + seat + ']';
                    component.set("v.afterRemove",conSeat);
                    $(this).addClass('booked');
                    $(this).addClass('bookedseat');
                    $(this).attr('data','booked');
                    $(this).attr('contact',contactId);
                    var conList = component.get("v.contactList");
                    for(var i = 0;i<conList.length;i++){
                        if(conList[i].Name == contactName){
                            conList[i].Name = contactName + '[' + seat + ']';
                            conList[i].Title = seat;
                        }
                    }
                    $(this).append('<span Id="' + seat + '" class="orangeBox">X</span>');
                    
                    var spanId = document.getElementById(seat);
                    console.log('spanId'+spanId);
                    
                    
                     $(this).hover(
                      function () {
                        $(spanId).show();
                      }, 
                      function () {
                        $(spanId).hide();
                      }
                    );
                    
					$(spanId).on('click' ,function(){
                    	alert('Hello');
                        debugger;
                        $(this).parent().removeClass('bookedseat');
                        $(this).parent().removeClass('booked');
                        $(this).parent().removeAttr('data');
                        $(this).parent().unbind('mouseenter');
                         $(this).parent().removeClass('orangeBox');
                        $(spanId).remove();


                        var a = $(this).parent();
                        var contactID = a.attr("contact")
                        var conList = component.get("v.contactList");
                    	for(var i = 0;i<conList.length;i++){
                        	if(conList[i].Id == contactID){
                            	//conList[i].Name = 'WWWWWW';//Mustafa will look into it
                            	//conList[i].Title = '';
                        	}
                    	}
                        //$(this).parent().trigger("drop");
                                                 
                         sObjectEvent1 = component.getEvent("getAssignSeat");
                    
                            sObjectEvent1.setParams({
                                "contactId":contactId,
                                "seat" :seat,
                                "tableNo":tableNo,
                                "status":"Cancelled"
                               })
                           sObjectEvent1.fire();
                      });
                     sObjectEvent1 = component.getEvent("getAssignSeat");
                    var status = "Registered";
                    sObjectEvent1.setParams({
                        "contactId":contactId,
                        "seat" :seat,
                        "tableNo":tableNo,
                        "status":status
                       })
                   sObjectEvent1.fire();
                }
                else{
                    alert("Sorry ! already booked...");
                }
             
            });
            $("ul[id='"+globalID+"_leftSideUl"+"']").append(leftSideSeat);
            var seatNumber = document.createElement('p');
            seatNumber.innerHTML = 'S'+i;
            seatNumber.className = 'p';
            leftSideSeat.appendChild(seatNumber);
            
        }
        var lSeats = document.getElementById(globalID+'_leftSeating');
        lSeats.style.height = 30*i+'px';
        /*******************************************************************************************/
        /************************** Horizontal TOP side seating Arrangement Start **********/
        
        
        var getHorizontalSeats = document.getElementById(globalID+'_horizontalSeatingTop');
        var horizontalSideSeats = document.createElement('ul');
        horizontalSideSeats.id = globalID + '_horizontalUlTop';
        
        
        getHorizontalSeats.appendChild(horizontalSideSeats);
        var k=0;
        var p=0;
        for(k=0;k<horizontalSeats;k++){
            p=i+k;
            var horizontalSeat =  document.createElement('li');
            horizontalSeat.id = globalID + '_horizontalSeat'+k;
            horizontalSeat.className = 'chair';
            $(horizontalSeat).on('dragover', function (event) {
                console.log('ondragover0 called...');
                event.preventDefault();
            });
           
            $(horizontalSeat).on('drop', function (event) {
                debugger;
                var sObjectEvent1;
                var obje = {};
          		 var status = $(this).attr('class');
                if(status.indexOf("booked") <= 0){
                    var listitem = '';
                    if(event.originalEvent != undefined && event.originalEvent != null){
                    	listitem = event.originalEvent.dataTransfer.getData("obj");    
                        obje = JSON.parse(listitem);
                    }
                    var contactId = obje.Id;
                    var contactName = obje.Name;
                    component.set("v.afterRemove",contactName);
                    var seatNo = $(this).find('p').text();
                    var tableNo = component.get("v.wrapperObj.tableName");
                    var seat = tableNo + '_'+  seatNo;
                    var conSeat = contactName + '[' + seat + ']';
                    component.set("v.afterRemove",conSeat);
                    $(this).addClass('booked');
                    $(this).addClass('bookedseat');
                    $(this).attr('data','booked');
                    $(this).attr('contact',contactId);
                    var conList = component.get("v.contactList");
                    for(var i = 0;i<conList.length;i++){
                        if(conList[i].Name == contactName){
                            conList[i].Name = contactName + '[' + seat + ']';
                            conList[i].Title = seat;
                        }
                    }
                    $(this).append('<span Id="' + seat + '" class="orangeBox">X</span>');
                    
                    var spanId = document.getElementById(seat);
                    console.log('spanId'+spanId);
                    
                   
                     $(this).hover(
                      function () {
                        $(spanId).show();
                      }, 
                      function () {
                        $(spanId).hide();
                      }
                    );
                    
					$(spanId).on('click' ,function(){
                    	alert('Hello');
                        debugger;
                        $(this).parent().removeClass('bookedseat');
                        $(this).parent().removeClass('booked');
                        $(this).parent().removeAttr('data');
                        $(this).parent().unbind('mouseenter');
                        $(this).parent().removeClass('orangeBox');
                        $(spanId).remove();

                        var a = $(this).parent();
                        var contactID = a.attr("contact")
                        var conList = component.get("v.contactList");
                    	for(var i = 0;i<conList.length;i++){
                        	if(conList[i].Id == contactID){
                            	//conList[i].Name = 'WWWWWW';//Mustafa will look into it
                            	//conList[i].Title = '';
                        	}
                    	}
                        //$(this).parent().trigger("drop");
                                                 
                         sObjectEvent1 = component.getEvent("getAssignSeat");
                    
                            sObjectEvent1.setParams({
                                "contactId":contactId,
                                "seat" :seat,
                                "tableNo":tableNo,
                                "status":"Cancelled"
                               })
                           sObjectEvent1.fire();
                      });
                     sObjectEvent1 = component.getEvent("getAssignSeat");
                    var status = "Registered";
                    sObjectEvent1.setParams({
                        "contactId":contactId,
                        "seat" :seat,
                        "tableNo":tableNo,
                        "status":status
                       })
                   sObjectEvent1.fire();
                }
                else{
                    alert("Sorry ! already booked...");
                }
             
            });
            $("ul[id='"+globalID+"_horizontalUlTop"+"']").append(horizontalSeat);
            var seatNumber = document.createElement('p');
            seatNumber.innerHTML = 'S'+p;
            seatNumber.className = 'p';
            horizontalSeat.appendChild(seatNumber);
            
        }
        var hSeats = document.getElementById(globalID+'_horizontalSeatingTop');
        hSeats.style.width = 30+30*k+'px';
        /******************************************************************************************************************/
        
        /************************** vertical Right side seating Arrangement Start **********/
        var getRightSeats = document.getElementById(globalID+'_rightSeating');
        var rightSideSeats = document.createElement('ul');
        rightSideSeats.id = globalID + '_rightSideUl';
        getRightSeats.appendChild(rightSideSeats);
        var j=0; 
        for(j=0;j<numberOfSeatsOnRightSide;j++){
            var r= p+numberOfSeatsOnRightSide-j;
            var rightSideSeat =  document.createElement('li');
            rightSideSeat.id = globalID + '_rightSideSeat'+j;
            rightSideSeat.className = 'chair';
            $(rightSideSeat).on('dragover', function (event) {
                console.log('ondragover0 called...');
                event.preventDefault();
            });
           
            $(rightSideSeat).on('drop', function (event) {
                debugger;
                var sObjectEvent1;
                var obje = {};
          		 var status = $(this).attr('class');
                if(status.indexOf("booked") <= 0){
                    var listitem = '';
                    if(event.originalEvent != undefined && event.originalEvent != null){
                    	listitem = event.originalEvent.dataTransfer.getData("obj");    
                        obje = JSON.parse(listitem);
                    }
                    var contactId = obje.Id;
                    var contactName = obje.Name;
                    component.set("v.afterRemove",contactName);
                    var seatNo = $(this).find('p').text();
                    var tableNo = component.get("v.wrapperObj.tableName");
                    var seat = tableNo + '_'+  seatNo;
                    var conSeat = contactName + '[' + seat + ']';
                    component.set("v.afterRemove",conSeat);
                    $(this).addClass('booked');
                    $(this).addClass('bookedseat');
                    $(this).attr('data','booked');
                    $(this).attr('contact',contactId);
                    var conList = component.get("v.contactList");
                    for(var i = 0;i<conList.length;i++){
                        if(conList[i].Name == contactName){
                            conList[i].Name = contactName + '[' + seat + ']';
                            conList[i].Title = seat;
                        }
                    }
                    $(this).append('<span Id="' + seat + '" class="orangeBox">X</span>');
                    
                    var spanId = document.getElementById(seat);
                    console.log('spanId'+spanId);
                    
                    
                     $(this).hover(
                      function () {
                        $(spanId).show();
                      }, 
                      function () {
                        $(spanId).hide();
                      }
                    );
                    
					$(spanId).on('click' ,function(){
                    	alert('Hello');
                        debugger;
                        $(this).parent().removeClass('bookedseat');
                        $(this).parent().removeClass('booked');
                        $(this).parent().removeAttr('data');
                        $(this).parent().unbind('mouseenter');
    					 $(this).parent().removeClass('orangeBox');
                        $(spanId).remove();

                        var a = $(this).parent();
                        var contactID = a.attr("contact")
                        var conList = component.get("v.contactList");
                    	for(var i = 0;i<conList.length;i++){
                        	if(conList[i].Id == contactID){
                            	//conList[i].Name = 'WWWWWW';//Mustafa will look into it
                            	//conList[i].Title = '';
                        	}
                    	}
                        //$(this).parent().trigger("drop");
                                                 
                         sObjectEvent1 = component.getEvent("getAssignSeat");
                    
                            sObjectEvent1.setParams({
                                "contactId":contactId,
                                "seat" :seat,
                                "tableNo":tableNo,
                                "status":"Cancelled"
                               })
                           sObjectEvent1.fire();
                      });
                     sObjectEvent1 = component.getEvent("getAssignSeat");
                    var status = "Registered";
                    sObjectEvent1.setParams({
                        "contactId":contactId,
                        "seat" :seat,
                        "tableNo":tableNo,
                        "status":status
                       })
                   sObjectEvent1.fire();
                }
                else{
                    alert("Sorry ! already booked...");
                }
             
            });
            $("ul[id='"+globalID+"_rightSideUl"+"']").append(rightSideSeat);
            var seatNumber = document.createElement('p');
            seatNumber.innerHTML = 'S'+r;
            seatNumber.className = 'p';
            rightSideSeat.appendChild(seatNumber);
            
        }
        
        var lSeats = document.getElementById(globalID+'_rightSeating');
        lSeats.style.height = 30*i+'px';
        lSeats.style.marginTop = 50+30*i*(-1)+'px';
        lSeats.style.marginLeft = 120+(30*horizontalSeats)+'px';
        
        
        
        /************************** Horizontal bottom side seating Arrangement Start **********/
        
        var getHorizontalSeats = document.getElementById(globalID+'_horizontalSeatingBottom');
        var horizontalSideSeats = document.createElement('ul');
        horizontalSideSeats.id = globalID + '_horizontalUlBottom';
        horizontalSideSeats.style.position = 'relative';
        horizontalSideSeats.style.top = -84+'px';
        getHorizontalSeats.appendChild(horizontalSideSeats);
        var k=0;
        for(k=0;k<horizontalSeats;k++){
            var s= r+numberOfSeatsOnRightSide+horizontalSeats-k-1;
            var horizontalSeat =  document.createElement('li');
            horizontalSeat.id = globalID + '_horizontalSeat'+k;
            horizontalSeat.className = 'chair';
            $(horizontalSeat).on('dragover', function (event) {
                console.log('ondragover0 called...');
                event.preventDefault();
            });
           
            $(horizontalSeat).on('drop', function (event) {
                debugger;
                var sObjectEvent1;
                var obje = {};
          		 var status = $(this).attr('class');
                if(status.indexOf("booked") <= 0){
                    var listitem = '';
                    if(event.originalEvent != undefined && event.originalEvent != null){
                    	listitem = event.originalEvent.dataTransfer.getData("obj");    
                        obje = JSON.parse(listitem);
                    }
                    var contactId = obje.Id;
                    var contactName = obje.Name;
                    component.set("v.afterRemove",contactName);
                    var seatNo = $(this).find('p').text();
                    var tableNo = component.get("v.wrapperObj.tableName");
                    var seat = tableNo + '_'+ seatNo;
                    var conSeat = contactName + '[' + seat + ']';
                    component.set("v.afterRemove",conSeat);
                    $(this).addClass('booked');
                    $(this).addClass('bookedseat');
                    $(this).attr('data','booked');
                    $(this).attr('contact',contactId);
                    var conList = component.get("v.contactList");
                    for(var i = 0;i<conList.length;i++){
                        if(conList[i].Name == contactName){
                            conList[i].Name = contactName + '[' + seat + ']';
                            conList[i].Title = seat;
                        }
                    }
                    $(this).append('<span Id="' + seat + '" class="orangeBox">X</span>');
                    
                    var spanId = document.getElementById(seat);
                    console.log('spanId'+spanId);
                    
                   
                     $(this).hover(
                      function () {
                        $(spanId).show();
                      }, 
                      function () {
                        $(spanId).hide();
                      }
                    );
                    
					$(spanId).on('click' ,function(){
                    	alert('Hello');
                        debugger;
                        $(this).parent().removeClass('bookedseat');
                        $(this).parent().removeClass('booked');
                        $(this).parent().removeAttr('data');
                        $(this).parent().unbind('mouseenter');
						 $(this).parent().removeClass('orangeBox');
                        $(spanId).remove();

                        var a = $(this).parent();
                        var contactID = a.attr("contact")
                        var conList = component.get("v.contactList");
                    	for(var i = 0;i<conList.length;i++){
                        	if(conList[i].Id == contactID){
                            	//conList[i].Name = 'WWWWWW';//Mustafa will look into it
                            	//conList[i].Title = '';
                        	}
                    	}
                        //$(this).parent().trigger("drop");
                                                 
                         sObjectEvent1 = component.getEvent("getAssignSeat");
                    
                            sObjectEvent1.setParams({
                                "contactId":contactId,
                                "seat" :seat,
                                "tableNo":tableNo,
                                "status":"Cancelled"
                               })
                           sObjectEvent1.fire();
                      });
                     sObjectEvent1 = component.getEvent("getAssignSeat");
                    var status = "Registered";
                    sObjectEvent1.setParams({
                        "contactId":contactId,
                        "seat" :seat,
                        "tableNo":tableNo,
                        "status":status
                       })
                   sObjectEvent1.fire();
                }
                else{
                    alert("Sorry ! already booked...");
                }
             
            });
            $("ul[id='"+globalID+"_horizontalUlBottom"+"']").append(horizontalSeat);
            var seatNumber = document.createElement('p');
            seatNumber.innerHTML = 'S'+s;
            seatNumber.className = 'p';
            horizontalSeat.appendChild(seatNumber);
        }
        var hSeats = document.getElementById(globalID+'_horizontalSeatingBottom');
        hSeats.style.width = 30+30*k+'px';
        hSeats.style.marginTop = (30*i)*(-1)+'px';
        
        
        
        
        
        
        
        // Horizontal seat arrangement end
        
        
        
        
    }
})