({
    drawBoothLayout : function(component) {
        
        debugger;
        var globalID = component.getGlobalId();
        console.log(component.getGlobalId());
        var numberOfSeats = component.get("v.wrapperObj.NumberOfSeats");
        var totalSeatCounter = 1; 		
        /* Edit Layout
           Added by: Ankush
           Date    : 13/07/2017 */
        
        var obj =  component.get("v.wrapperObj");
        var stion = component.get("v.section");
        console.log("obj+++++++" +obj);
        var parentdiv = document.getElementById(globalID); 
        parentdiv.addEventListener("click",$A.getCallback(function myFunction(component,event){
            var sObjectEvent = $A.get("e.c:EditLayoutEvent");
            sObjectEvent.setParams({
                "message" : obj
            });
            sObjectEvent.fire();
        }));
        
        
        /*Edit Layout End*/ 
        
        
        // booth
        
        var capacity = component.get("v.wrapperObj.NumberOfSeats");//50;
        var seatsPerRow = component.get("v.wrapperObj.SeatsPerRow");//10;
        var numberOfRows = Math.round(capacity/seatsPerRow);
        //var row = 1;
        for(var cap=0;cap < numberOfRows;cap++){
            //$.each(rows, function(tableNo) {
            var audienceTemplate =  $('#audience-layout-template').html();
            var context = $(audienceTemplate);
            var rowTemplate = document.getElementById(globalID + '_seat-template');
            var rowLi = document.createElement('li');
            rowLi.id = globalID + '_rowLi';
            var rowUl = document.createElement('ul');
            rowUl.id = globalID + '_rowUl';
            rowUl.style.margin = '3px';
            rowTemplate.appendChild(rowLi);
            
            for(var row=0;row<seatsPerRow;row++){
                if(totalSeatCounter <= numberOfSeats ){
                    
                    var seatLi = document.createElement('li');
                    seatLi.className = 'chair';
                    rowUl.appendChild(seatLi);
                    var label = document.createElement('label')
                    label.htmlFor = "seatLabel"+totalSeatCounter;
                    label.className = "Seatlabel";
                    label.innerHTML = 'S'+totalSeatCounter;
                    seatLi.appendChild(label);
                    $(seatLi).on('dragover', function (event) {
                        console.log('ondragover0 called...');
                        event.preventDefault();
                    });
                    
                    $(seatLi).on('drop', function (event) {
                        debugger;
                        var sObjectEvent1;
                        var obje = {};
                        var status = $(this).attr('class');
                        if(status.indexOf("booked") <= 0){
                            var listitem = '';
                            if(event.originalEvent != undefined && event.originalEvent != null){
                                listitem = event.originalEvent.dataTransfer.getData("obj");    
                                obje = JSON.parse(listitem);
                            }
                            var contactId = obje.Id;
                            var contactName = obje.Name;
                            component.set("v.afterRemove",contactName);
                            var seatNo = $(this).find('label').text();
                            var tableNo = component.get("v.wrapperObj.tableName");
                            var seat = tableNo + '_'+ seatNo;
                            var conSeat = contactName + '[' + seat + ']';
                            component.set("v.afterRemove",conSeat);
                            $(this).addClass('booked');
                            $(this).addClass('bookedseat');
                            $(this).attr('data','booked');
                            $(this).attr('contact',contactId);
                            var conList = component.get("v.contactList");
                            for(var i = 0;i<conList.length;i++){
                                if(conList[i].Name == contactName){
                                    conList[i].Name = contactName + '[' + seat + ']';
                                    conList[i].Title = seat;
                                }
                            }
                            $(this).append('<span Id="' + seat + '" class="orangeBox">X</span>');
                            
                            var spanId = document.getElementById(seat);
                            console.log('spanId'+spanId);
                            $(this).hover(
                                function () {
                                    $(spanId).show();
                                }, 
                                function () {
                                    $(spanId).hide();
                                }
                            );
                            
                            $(spanId).on('click' ,function(){
                                alert('Hello');
                                debugger;
                                $(this).parent().removeClass('booked');
                                $(this).parent().removeClass('bookedseat');
                                $(this).parent().removeAttr('data');
                                $(this).parent().unbind('mouseenter');
                                $(this).parent().removeClass('orangeBox');
                                $(spanId).remove();
                                var a = $(this).parent();
                                var contactID = a.attr("contact")
                                var conList = component.get("v.contactList");
                                for(var i = 0;i<conList.length;i++){
                                    if(conList[i].Id == contactID){
                                        //conList[i].Name = 'WWWWWW';//Mustafa will look into it
                                        //conList[i].Title = '';
                                    }
                                }
                                //$(this).parent().trigger("drop");
                                
                                sObjectEvent1 = component.getEvent("getAssignSeat");
                                
                                sObjectEvent1.setParams({
                                    "contactId":contactId,
                                    "seat" :seat,
                                    "tableNo":tableNo,
                                    "status":"Cancelled"
                                })
                                sObjectEvent1.fire();
                            });
                            sObjectEvent1 = component.getEvent("getAssignSeat");
                            var status = "Registered";
                            sObjectEvent1.setParams({
                                "contactId":contactId,
                                "seat" :seat,
                                "tableNo":tableNo,
                                "status":status
                            })
                            sObjectEvent1.fire();
                        }
                        else{
                            alert("Sorry ! already booked...");
                        }
                        
                    });
                    totalSeatCounter++;
                }
            }
            rowLi.appendChild(rowUl);
            $("ul[id='"+globalID+"_seat-template"+"']").append(rowLi);
            //  row++;
        }
        
    }
})